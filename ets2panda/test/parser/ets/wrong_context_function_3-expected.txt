{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "a",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 16,
                  "column": 10
                },
                "end": {
                  "line": 16,
                  "column": 11
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "a",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 16,
                      "column": 10
                    },
                    "end": {
                      "line": 16,
                      "column": 11
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 1
                    },
                    "end": {
                      "line": 19,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 11
                  },
                  "end": {
                    "line": 19,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 16,
                  "column": 11
                },
                "end": {
                  "line": 19,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 1
              },
              "end": {
                "line": 19,
                "column": 2
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 20,
                  "column": 10
                },
                "end": {
                  "line": 20,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 20,
                      "column": 10
                    },
                    "end": {
                      "line": 20,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ExpressionStatement",
                      "expression": {
                        "type": "UpdateExpression",
                        "operator": "++",
                        "prefix": false,
                        "argument": {
                          "type": "Identifier",
                          "name": "a",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 22,
                              "column": 5
                            },
                            "end": {
                              "line": 22,
                              "column": 6
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 22,
                            "column": 5
                          },
                          "end": {
                            "line": 22,
                            "column": 8
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 22,
                          "column": 5
                        },
                        "end": {
                          "line": 22,
                          "column": 8
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 21,
                      "column": 1
                    },
                    "end": {
                      "line": 23,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 20,
                    "column": 14
                  },
                  "end": {
                    "line": 23,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 20,
                  "column": 14
                },
                "end": {
                  "line": 23,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 20,
                "column": 1
              },
              "end": {
                "line": 23,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 24,
      "column": 1
    }
  }
}
TypeError: Function name "a" used in the wrong context [wrong_context_function_3.ets:22:5]
