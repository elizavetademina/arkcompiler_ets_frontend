{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 16,
              "column": 7
            },
            "end": {
              "line": 16,
              "column": 8
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "name",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 17,
                  "column": 5
                },
                "end": {
                  "line": 17,
                  "column": 9
                }
              }
            },
            "accessibility": "public",
            "static": false,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSFunctionType",
              "params": [],
              "returnType": {
                "type": "ETSTypeReference",
                "part": {
                  "type": "ETSTypeReferencePart",
                  "name": {
                    "type": "Identifier",
                    "name": "String",
                    "decorators": [],
                    "loc": {
                      "start": {
                        "line": 17,
                        "column": 17
                      },
                      "end": {
                        "line": 17,
                        "column": 23
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 17,
                      "column": 17
                    },
                    "end": {
                      "line": 18,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 17,
                    "column": 17
                  },
                  "end": {
                    "line": 18,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 17,
                  "column": 11
                },
                "end": {
                  "line": 18,
                  "column": 2
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 5
              },
              "end": {
                "line": 18,
                "column": 2
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 2
              },
              "end": {
                "line": 18,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 9
          },
          "end": {
            "line": 18,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 18,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "B",
          "decorators": [],
          "loc": {
            "start": {
              "line": 20,
              "column": 7
            },
            "end": {
              "line": 20,
              "column": 8
            }
          }
        },
        "superClass": {
          "type": "ETSTypeReference",
          "part": {
            "type": "ETSTypeReferencePart",
            "name": {
              "type": "Identifier",
              "name": "A",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 20,
                  "column": 17
                },
                "end": {
                  "line": 20,
                  "column": 18
                }
              }
            },
            "loc": {
              "start": {
                "line": 20,
                "column": 17
              },
              "end": {
                "line": 20,
                "column": 20
              }
            }
          },
          "loc": {
            "start": {
              "line": 20,
              "column": 17
            },
            "end": {
              "line": 20,
              "column": 20
            }
          }
        },
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "name",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 21,
                  "column": 5
                },
                "end": {
                  "line": 21,
                  "column": 9
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "name",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 21,
                      "column": 5
                    },
                    "end": {
                      "line": 21,
                      "column": 9
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 21,
                      "column": 13
                    },
                    "end": {
                      "line": 21,
                      "column": 16
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 21,
                      "column": 17
                    },
                    "end": {
                      "line": 21,
                      "column": 19
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 21,
                    "column": 9
                  },
                  "end": {
                    "line": 21,
                    "column": 19
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 21,
                  "column": 9
                },
                "end": {
                  "line": 21,
                  "column": 19
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 21,
                "column": 5
              },
              "end": {
                "line": 21,
                "column": 19
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 22,
                "column": 2
              },
              "end": {
                "line": 22,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 20,
            "column": 19
          },
          "end": {
            "line": 22,
            "column": 2
          }
        }
      },
      "loc": {
        "start": {
          "line": 20,
          "column": 1
        },
        "end": {
          "line": 22,
          "column": 2
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 23,
      "column": 1
    }
  }
}
TypeError: Cannot inherit from class A, because method name is inherited with a different declaration type [invalidInheritance4.ets:20:17]
