{
  "type": "Program",
  "statements": [
    {
      "type": "TSEnumDeclaration",
      "id": {
        "type": "Identifier",
        "name": "Color",
        "decorators": [],
        "loc": {
          "start": {
            "line": 16,
            "column": 6
          },
          "end": {
            "line": 16,
            "column": 11
          }
        }
      },
      "members": [
        {
          "type": "TSEnumMember",
          "id": {
            "type": "Identifier",
            "name": "Red",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 14
              },
              "end": {
                "line": 16,
                "column": 17
              }
            }
          },
          "initializer": {
            "type": "NumberLiteral",
            "value": 0,
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 14
            },
            "end": {
              "line": 16,
              "column": 17
            }
          }
        },
        {
          "type": "TSEnumMember",
          "id": {
            "type": "Identifier",
            "name": "Green",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 19
              },
              "end": {
                "line": 16,
                "column": 24
              }
            }
          },
          "initializer": {
            "type": "NumberLiteral",
            "value": 1,
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 19
            },
            "end": {
              "line": 16,
              "column": 24
            }
          }
        },
        {
          "type": "TSEnumMember",
          "id": {
            "type": "Identifier",
            "name": "Blue",
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 26
              },
              "end": {
                "line": 16,
                "column": 30
              }
            }
          },
          "initializer": {
            "type": "NumberLiteral",
            "value": 2,
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          "loc": {
            "start": {
              "line": 16,
              "column": 26
            },
            "end": {
              "line": 16,
              "column": 30
            }
          }
        }
      ],
      "const": false,
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 32
        }
      }
    },
    {
      "type": "TSTypeAliasDeclaration",
      "id": {
        "type": "Identifier",
        "name": "BaseColor",
        "decorators": [],
        "loc": {
          "start": {
            "line": 17,
            "column": 6
          },
          "end": {
            "line": 17,
            "column": 15
          }
        }
      },
      "typeAnnotation": {
        "type": "ETSTypeReference",
        "part": {
          "type": "ETSTypeReferencePart",
          "name": {
            "type": "Identifier",
            "name": "Color",
            "decorators": [],
            "loc": {
              "start": {
                "line": 17,
                "column": 18
              },
              "end": {
                "line": 17,
                "column": 23
              }
            }
          },
          "loc": {
            "start": {
              "line": 17,
              "column": 18
            },
            "end": {
              "line": 17,
              "column": 24
            }
          }
        },
        "loc": {
          "start": {
            "line": 17,
            "column": 18
          },
          "end": {
            "line": 17,
            "column": 24
          }
        }
      },
      "loc": {
        "start": {
          "line": 17,
          "column": 1
        },
        "end": {
          "line": 17,
          "column": 24
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 19,
                  "column": 10
                },
                "end": {
                  "line": 19,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 19,
                      "column": 10
                    },
                    "end": {
                      "line": 19,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 19,
                      "column": 18
                    },
                    "end": {
                      "line": 19,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "a",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "Color",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 20,
                                      "column": 12
                                    },
                                    "end": {
                                      "line": 20,
                                      "column": 17
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 20,
                                    "column": 12
                                  },
                                  "end": {
                                    "line": 20,
                                    "column": 19
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 20,
                                  "column": 12
                                },
                                "end": {
                                  "line": 20,
                                  "column": 19
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 20,
                                "column": 9
                              },
                              "end": {
                                "line": 20,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "MemberExpression",
                            "object": {
                              "type": "Identifier",
                              "name": "Color",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 20,
                                  "column": 20
                                },
                                "end": {
                                  "line": 20,
                                  "column": 25
                                }
                              }
                            },
                            "property": {
                              "type": "Identifier",
                              "name": "Red",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 20,
                                  "column": 26
                                },
                                "end": {
                                  "line": 20,
                                  "column": 29
                                }
                              }
                            },
                            "computed": false,
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 20,
                                "column": 20
                              },
                              "end": {
                                "line": 20,
                                "column": 29
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 20,
                              "column": 9
                            },
                            "end": {
                              "line": 20,
                              "column": 29
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 20,
                          "column": 5
                        },
                        "end": {
                          "line": 20,
                          "column": 30
                        }
                      }
                    },
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "v",
                            "typeAnnotation": {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "BaseColor",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 21,
                                      "column": 12
                                    },
                                    "end": {
                                      "line": 21,
                                      "column": 21
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 21,
                                    "column": 12
                                  },
                                  "end": {
                                    "line": 21,
                                    "column": 23
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 21,
                                  "column": 12
                                },
                                "end": {
                                  "line": 21,
                                  "column": 23
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 21,
                                "column": 9
                              },
                              "end": {
                                "line": 21,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "MemberExpression",
                            "object": {
                              "type": "Identifier",
                              "name": "BaseColor",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 21,
                                  "column": 24
                                },
                                "end": {
                                  "line": 21,
                                  "column": 33
                                }
                              }
                            },
                            "property": {
                              "type": "Identifier",
                              "name": "Red",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 21,
                                  "column": 34
                                },
                                "end": {
                                  "line": 21,
                                  "column": 37
                                }
                              }
                            },
                            "computed": false,
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 21,
                                "column": 24
                              },
                              "end": {
                                "line": 21,
                                "column": 37
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 21,
                              "column": 9
                            },
                            "end": {
                              "line": 21,
                              "column": 37
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 21,
                          "column": 5
                        },
                        "end": {
                          "line": 21,
                          "column": 38
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 19,
                      "column": 23
                    },
                    "end": {
                      "line": 22,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 19,
                    "column": 14
                  },
                  "end": {
                    "line": 22,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 19,
                  "column": 14
                },
                "end": {
                  "line": 22,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 19,
                "column": 1
              },
              "end": {
                "line": 22,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 24,
      "column": 1
    }
  }
}
TypeError: Cannot refer to enum members through type alias. [enum_as_type_alias.ets:21:34]
