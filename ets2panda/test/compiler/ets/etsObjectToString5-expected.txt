{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "A",
          "decorators": [],
          "loc": {
            "start": {
              "line": 16,
              "column": 7
            },
            "end": {
              "line": 16,
              "column": 8
            }
          }
        },
        "typeParameters": {
          "type": "TSTypeParameterDeclaration",
          "params": [
            {
              "type": "TSTypeParameter",
              "name": {
                "type": "Identifier",
                "name": "T",
                "decorators": [],
                "loc": {
                  "start": {
                    "line": 16,
                    "column": 9
                  },
                  "end": {
                    "line": 16,
                    "column": 10
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 16,
                  "column": 9
                },
                "end": {
                  "line": 16,
                  "column": 11
                }
              }
            }
          ],
          "loc": {
            "start": {
              "line": 16,
              "column": 8
            },
            "end": {
              "line": 16,
              "column": 11
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "constructor",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "constructor",
            "static": false,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "constructor",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 16,
                "column": 13
              },
              "end": {
                "line": 16,
                "column": 13
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 16,
            "column": 11
          },
          "end": {
            "line": 16,
            "column": 13
          }
        }
      },
      "loc": {
        "start": {
          "line": 16,
          "column": 1
        },
        "end": {
          "line": 16,
          "column": 13
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "_$init$_",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "_$init$_",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "body": {
                  "type": "BlockStatement",
                  "statements": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 1
                    },
                    "end": {
                      "line": 1,
                      "column": 1
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 1
                  },
                  "end": {
                    "line": 1,
                    "column": 1
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 1
                },
                "end": {
                  "line": 1,
                  "column": 1
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 1,
                "column": 1
              }
            }
          },
          {
            "type": "ClassProperty",
            "key": {
              "type": "Identifier",
              "name": "g",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 5
                },
                "end": {
                  "line": 18,
                  "column": 6
                }
              }
            },
            "value": {
              "type": "ArrowFunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": null,
                "generator": false,
                "async": false,
                "expression": false,
                "params": [
                  {
                    "type": "ETSParameterExpression",
                    "name": {
                      "type": "Identifier",
                      "name": "y",
                      "typeAnnotation": {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "A",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 45
                              },
                              "end": {
                                "line": 18,
                                "column": 46
                              }
                            }
                          },
                          "typeParams": {
                            "type": "TSTypeParameterInstantiation",
                            "params": [
                              {
                                "type": "ETSTypeReference",
                                "part": {
                                  "type": "ETSTypeReferencePart",
                                  "name": {
                                    "type": "Identifier",
                                    "name": "String",
                                    "decorators": [],
                                    "loc": {
                                      "start": {
                                        "line": 18,
                                        "column": 47
                                      },
                                      "end": {
                                        "line": 18,
                                        "column": 53
                                      }
                                    }
                                  },
                                  "loc": {
                                    "start": {
                                      "line": 18,
                                      "column": 47
                                    },
                                    "end": {
                                      "line": 18,
                                      "column": 54
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 18,
                                    "column": 47
                                  },
                                  "end": {
                                    "line": 18,
                                    "column": 54
                                  }
                                }
                              }
                            ],
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 46
                              },
                              "end": {
                                "line": 18,
                                "column": 54
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 45
                            },
                            "end": {
                              "line": 18,
                              "column": 55
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 45
                          },
                          "end": {
                            "line": 18,
                            "column": 55
                          }
                        }
                      },
                      "decorators": [],
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 43
                        },
                        "end": {
                          "line": 18,
                          "column": 55
                        }
                      }
                    },
                    "loc": {
                      "start": {
                        "line": 18,
                        "column": 43
                      },
                      "end": {
                        "line": 18,
                        "column": 55
                      }
                    }
                  }
                ],
                "returnType": {
                  "type": "ETSUnionType",
                  "types": [
                    {
                      "type": "ETSTypeReference",
                      "part": {
                        "type": "ETSTypeReferencePart",
                        "name": {
                          "type": "Identifier",
                          "name": "A",
                          "decorators": [],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 57
                            },
                            "end": {
                              "line": 18,
                              "column": 58
                            }
                          }
                        },
                        "typeParams": {
                          "type": "TSTypeParameterInstantiation",
                          "params": [
                            {
                              "type": "ETSTypeReference",
                              "part": {
                                "type": "ETSTypeReferencePart",
                                "name": {
                                  "type": "Identifier",
                                  "name": "String",
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 18,
                                      "column": 59
                                    },
                                    "end": {
                                      "line": 18,
                                      "column": 65
                                    }
                                  }
                                },
                                "loc": {
                                  "start": {
                                    "line": 18,
                                    "column": 59
                                  },
                                  "end": {
                                    "line": 18,
                                    "column": 66
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 18,
                                  "column": 59
                                },
                                "end": {
                                  "line": 18,
                                  "column": 66
                                }
                              }
                            }
                          ],
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 58
                            },
                            "end": {
                              "line": 18,
                              "column": 66
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 57
                          },
                          "end": {
                            "line": 18,
                            "column": 67
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 57
                        },
                        "end": {
                          "line": 18,
                          "column": 67
                        }
                      }
                    },
                    {
                      "type": "ETSNullType",
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 67
                        },
                        "end": {
                          "line": 18,
                          "column": 71
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 57
                    },
                    "end": {
                      "line": 18,
                      "column": 71
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "ReturnStatement",
                      "argument": {
                        "type": "Identifier",
                        "name": "y",
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 19,
                            "column": 12
                          },
                          "end": {
                            "line": 19,
                            "column": 13
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 19,
                          "column": 5
                        },
                        "end": {
                          "line": 19,
                          "column": 14
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 75
                    },
                    "end": {
                      "line": 20,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 18,
                    "column": 42
                  },
                  "end": {
                    "line": 20,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 18,
                  "column": 42
                },
                "end": {
                  "line": 20,
                  "column": 2
                }
              }
            },
            "accessibility": "public",
            "static": true,
            "readonly": false,
            "declare": false,
            "optional": false,
            "computed": false,
            "typeAnnotation": {
              "type": "ETSUnionType",
              "types": [
                {
                  "type": "ETSFunctionType",
                  "params": [
                    {
                      "type": "ETSParameterExpression",
                      "name": {
                        "type": "Identifier",
                        "name": "y",
                        "typeAnnotation": {
                          "type": "ETSTypeReference",
                          "part": {
                            "type": "ETSTypeReferencePart",
                            "name": {
                              "type": "Identifier",
                              "name": "A",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 18,
                                  "column": 11
                                },
                                "end": {
                                  "line": 18,
                                  "column": 12
                                }
                              }
                            },
                            "typeParams": {
                              "type": "TSTypeParameterInstantiation",
                              "params": [
                                {
                                  "type": "ETSPrimitiveType",
                                  "loc": {
                                    "start": {
                                      "line": 18,
                                      "column": 13
                                    },
                                    "end": {
                                      "line": 18,
                                      "column": 16
                                    }
                                  }
                                }
                              ],
                              "loc": {
                                "start": {
                                  "line": 18,
                                  "column": 12
                                },
                                "end": {
                                  "line": 18,
                                  "column": 17
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 11
                              },
                              "end": {
                                "line": 18,
                                "column": 18
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 11
                            },
                            "end": {
                              "line": 18,
                              "column": 18
                            }
                          }
                        },
                        "decorators": [],
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 9
                          },
                          "end": {
                            "line": 18,
                            "column": 18
                          }
                        }
                      },
                      "loc": {
                        "start": {
                          "line": 18,
                          "column": 9
                        },
                        "end": {
                          "line": 18,
                          "column": 18
                        }
                      }
                    }
                  ],
                  "returnType": {
                    "type": "ETSUnionType",
                    "types": [
                      {
                        "type": "ETSTypeReference",
                        "part": {
                          "type": "ETSTypeReferencePart",
                          "name": {
                            "type": "Identifier",
                            "name": "A",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 22
                              },
                              "end": {
                                "line": 18,
                                "column": 23
                              }
                            }
                          },
                          "typeParams": {
                            "type": "TSTypeParameterInstantiation",
                            "params": [
                              {
                                "type": "ETSPrimitiveType",
                                "loc": {
                                  "start": {
                                    "line": 18,
                                    "column": 24
                                  },
                                  "end": {
                                    "line": 18,
                                    "column": 27
                                  }
                                }
                              }
                            ],
                            "loc": {
                              "start": {
                                "line": 18,
                                "column": 23
                              },
                              "end": {
                                "line": 18,
                                "column": 28
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 18,
                              "column": 22
                            },
                            "end": {
                              "line": 18,
                              "column": 29
                            }
                          }
                        },
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 22
                          },
                          "end": {
                            "line": 18,
                            "column": 29
                          }
                        }
                      },
                      {
                        "type": "ETSNullType",
                        "loc": {
                          "start": {
                            "line": 18,
                            "column": 29
                          },
                          "end": {
                            "line": 18,
                            "column": 33
                          }
                        }
                      }
                    ],
                    "loc": {
                      "start": {
                        "line": 18,
                        "column": 22
                      },
                      "end": {
                        "line": 18,
                        "column": 33
                      }
                    }
                  },
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 7
                    },
                    "end": {
                      "line": 18,
                      "column": 33
                    }
                  }
                },
                {
                  "type": "ETSNullType",
                  "loc": {
                    "start": {
                      "line": 18,
                      "column": 35
                    },
                    "end": {
                      "line": 18,
                      "column": 39
                    }
                  }
                }
              ],
              "loc": {
                "start": {
                  "line": 18,
                  "column": 7
                },
                "end": {
                  "line": 18,
                  "column": 39
                }
              }
            },
            "definite": false,
            "decorators": [],
            "loc": {
              "start": {
                "line": 18,
                "column": 5
              },
              "end": {
                "line": 20,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 21,
      "column": 1
    }
  }
}
TypeError: Type '(y: A<String>) => A<String>|null' cannot be assigned to type '(p1: A<Int>) => A<Int>|null|null' [etsObjectToString5.ets:18:42]
