/*
 * Copyright (c) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import q from 'assert';
const { codes1: { ERR_INVALID_ARG_TYPE1: a, ERR_MISSING_ARGS1: b, ERROR_OUT_OF_RANGE1: c }, AbortError1: d, } = require('./exportsApi1_toplevel');
q(a === 'ERR_INVALID_ARG_TYPE', 'success');
q(b === 'ERR_MISSING_ARGS', 'success');
q(c === 'ERROR_OUT_OF_RANGE', 'success');
let r = new d("hello");
q(r.message === 'hello', 'success');
